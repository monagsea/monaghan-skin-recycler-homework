package com.example.myapplication.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.myapplication.model.CategoryRepo
import kotlinx.coroutines.launch

class CategoryViewModel : ViewModel() {
    private val repo = CategoryRepo

    private val _list = MutableLiveData<List<String>>()
    val list: LiveData<List<String>> get() = _list

    fun getList() {
        viewModelScope.launch {
            val itemList = repo.getCategories()
            _list.value = itemList
        }
    }

}