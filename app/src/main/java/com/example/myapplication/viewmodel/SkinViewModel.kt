package com.example.myapplication.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.myapplication.model.SkinRepo
import kotlinx.coroutines.launch

class SkinViewModel : ViewModel() {
    private val repo = SkinRepo

    private val _list = MutableLiveData<List<String>>()
    val list: LiveData<List<String>> get() = _list

    fun getSkin() {
        viewModelScope.launch {
            val itemList = repo.getSkin()
            _list.value = itemList
        }
    }
}