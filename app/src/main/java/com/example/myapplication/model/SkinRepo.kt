package com.example.myapplication.model

import com.example.myapplication.model.remote.SkinApi

object SkinRepo : SkinApi {

    private val skinList = listOf<String>(
        "Normal",
        "Dry",
        "Oily",
        "Combine",
        "Flaky",
        "Decayed",
        "Rotten",
        "Malnourished",
        "Falling Off",
        "Jaundice"
    )

    override suspend fun getSkin(): List<String> {
        return skinList
    }
}