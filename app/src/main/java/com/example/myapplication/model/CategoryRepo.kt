package com.example.myapplication.model

import com.example.myapplication.model.remote.CategoryApi

object CategoryRepo: CategoryApi {

    private val categoryList = listOf<String>(
        "Ranked",
        "Hot",
        "Loved",
        "Secret",
        "Omega",
        "Unknown",
        "Cryptic",
        "Eldritch",
        "Horrific",
        "Outlawed"
    )

    override suspend fun getCategories(): List<String> {
        return categoryList
    }
}