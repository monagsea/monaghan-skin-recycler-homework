package com.example.myapplication.model.remote

interface SkinApi {
    suspend fun getSkin(): List<String>
}