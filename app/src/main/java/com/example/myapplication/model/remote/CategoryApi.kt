package com.example.myapplication.model.remote

interface CategoryApi {
    suspend fun getCategories(): List<String>
}