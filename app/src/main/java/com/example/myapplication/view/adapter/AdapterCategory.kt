package com.example.myapplication.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.databinding.CategoryListBinding
import com.example.myapplication.view.HomeFragmentDirections

class AdapterCategory : RecyclerView.Adapter<AdapterCategory.CategoryViewHolder>() {
    private var categories = listOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        val binding = CategoryListBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return CategoryViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        val category = categories[position]
        holder.loadDisplay(category)
    }

    override fun getItemCount(): Int {
        return categories.size
    }

    fun addCategories(categories: List<String>) {
        this.categories = categories.toMutableList()
        notifyDataSetChanged()
    }

    class CategoryViewHolder(
        private val binding: CategoryListBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun loadDisplay(category: String) {
            binding.tvList.text = category
            binding.imgCategory.setOnClickListener {
                it.findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToDetailFragment(category))
            }
        }
    }
}