package com.example.myapplication.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.databinding.SkinListBinding
import com.example.myapplication.view.HomeFragmentDirections

class AdapterSkin : RecyclerView.Adapter<AdapterSkin.SkinViewHolder>() {
    private var skins = listOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SkinViewHolder {
        val binding = SkinListBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return SkinViewHolder(binding)
    }

    override fun onBindViewHolder(holder: SkinViewHolder, position: Int) {
        val skin = skins[position]
        holder.loadDisplay(skin)
    }

    override fun getItemCount(): Int {
        return skins.size
    }

    fun addSkins(skins: List<String>) {
        this.skins = skins.toMutableList()
        notifyDataSetChanged()
    }

    class SkinViewHolder(
        private val binding: SkinListBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun loadDisplay(skin: String) {
            binding.tvList.text = skin
            binding.imgCategory.setOnClickListener {
                it.findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToDetailFragment(skin))
            }
        }
    }

}