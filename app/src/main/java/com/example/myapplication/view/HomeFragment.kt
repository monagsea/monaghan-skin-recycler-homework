package com.example.myapplication.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.myapplication.databinding.FragmentHomeBinding
import com.example.myapplication.view.adapter.AdapterCategory
import com.example.myapplication.view.adapter.AdapterSkin
import com.example.myapplication.viewmodel.CategoryViewModel
import com.example.myapplication.viewmodel.SkinViewModel


class HomeFragment : Fragment() {
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!
    private val categoryViewModel by viewModels<CategoryViewModel>()
    private val skinViewModel by viewModels<SkinViewModel>()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentHomeBinding.inflate(
        inflater, container, false
    ).also { _binding = it }.root

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fun displayCategories() =
            with(binding.rvList) {
                layoutManager =
                    LinearLayoutManager(this.context, LinearLayoutManager.HORIZONTAL, false)
                adapter = AdapterCategory().apply {
                    with(categoryViewModel) {
                        getList()
                        list.observe(viewLifecycleOwner) {
                            addCategories(it)
                        }
                    }
                }
            }

        fun displaySkin() =
            with(binding.rvSkinType) {
                layoutManager =
                    LinearLayoutManager(this.context, LinearLayoutManager.HORIZONTAL, false)
                adapter = AdapterSkin().apply {
                    with(skinViewModel) {
                        getSkin()
                        list.observe(viewLifecycleOwner) {
                            addSkins(it)
                        }
                    }
                }
            }
        displayCategories()
        displaySkin()
    }
}